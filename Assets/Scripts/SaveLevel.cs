﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SaveLevel : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        PlayerPrefs.SetInt("level", SceneManager.GetActiveScene().buildIndex);
        PlayerPrefs.Save();
    }
}
