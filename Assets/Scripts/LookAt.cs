﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour
{

    public Transform target;

    public bool findPlayer = false;

    void Start()
    {
        if (findPlayer)
            target = FindObjectOfType<Gun>().gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.LookAt(target);
    }
}
