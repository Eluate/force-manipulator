﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

    public bool killsPlayer = false;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Player") && !killsPlayer)
        {
            return;
        }

        Destroy(collision.gameObject);
    }
}
