﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    [SerializeField]
    private AudioClip projectileHitSound;
    [SerializeField]
    private AudioClip enemyHitSound;

    //[SerializeField]
    //private float delay = 1f;

    private bool canDestroy = false;

    void Start()
    {
        StartCoroutine(DestroyActivation());
    }

    void OnTriggerEnter(Collider collision)
    {
        string name = collision.gameObject.name;

        if (name.Contains("Enemy") || name.Contains("Bullet"))
        {
            if (!canDestroy || gameObject.tag != "f")
            {
                return;
            }
        }

        if (name == "Player Controller")
        {
            MapGenerator.instance.PlayerKilled();
            Destroy(gameObject);
        }
        else if (name.Contains("Enemy"))
        {
            MapGenerator.instance.EnemyDestroyed();
            AudioSource.PlayClipAtPoint(enemyHitSound, gameObject.transform.position, 0.5f);
            Destroy(collision.transform.parent.gameObject);
            Destroy(gameObject);
        }
        else
        {
            if (!name.Contains("Coin") && !name.Contains("Sphere"))
            {
                StartCoroutine(DelayedDestroy());
            }
        }
    }

    IEnumerator DelayedDestroy()
    {
        AudioSource.PlayClipAtPoint(projectileHitSound, gameObject.transform.position, 0.01f);

        //yield return new WaitForSeconds(delay);
      
        Destroy(gameObject);

        yield return 0;
    }

    IEnumerator DestroyActivation ()
    {
        yield return new WaitForSeconds(0.5f);

        canDestroy = true;

        yield return 0;
    }
}
