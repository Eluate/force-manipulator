﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour {

    public float xSpeed = 0.0f;
    public float ySpeed = 0.0f;
    public float zSpeed = 0.0f;

    public bool randomBounds = false;
    public bool canInvert = false;

    void Awake()
    {
        if (randomBounds)
        {
            xSpeed = Random.Range(-xSpeed, xSpeed);
            ySpeed = Random.Range(-ySpeed, ySpeed);
            zSpeed = Random.Range(-zSpeed, zSpeed);
        }

        if (canInvert)
        {
            if (Random.Range(0, 2) == 1)
            {
                xSpeed = -xSpeed;
            }
            if (Random.Range(0, 2) == 1)
            {
                ySpeed = -ySpeed;
            }
            if (Random.Range(0, 2) == 1)
            {
                zSpeed = -zSpeed;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(
             xSpeed * Time.deltaTime,
             ySpeed * Time.deltaTime,
             zSpeed * Time.deltaTime
        );
    }
}
