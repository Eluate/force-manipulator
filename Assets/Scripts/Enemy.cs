﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    // Projectile Variables
    public Vector3 projectileDirection;
    public float projectileRateOfFire;
    public GameObject projectile;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Player"))
        {
            return;
        }

        MapGenerator.instance.EnemyDestroyed();
        Destroy(gameObject);
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.Contains("Player"))
        {
            return;
        }

        MapGenerator.instance.EnemyDestroyed();
        Destroy(gameObject);
    }

    // Use this for initialization
    IEnumerator Start ()
    {
        // Have a random delay so that not all enemies fire in one frame
        yield return new WaitForSeconds(Random.Range(0.0f, 2.0f));
        StartCoroutine(Shoot());
	}

    IEnumerator Shoot ()
    {
        // TODO: Play sound
        Instantiate(projectile, transform.position, /*(Quaternion.LookRotation(projectileDirection) **/ transform.rotation);

        yield return new WaitForSeconds(projectileRateOfFire);
        StartCoroutine(Shoot());
    }
}
