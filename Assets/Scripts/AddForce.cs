﻿using UnityEngine;
using System.Collections;

public class AddForce : MonoBehaviour
{
    [SerializeField]
    private int force;

    // Use this for initialization
    void Start()
    {
        Vector3 direction = transform.forward;
        GetComponent<Rigidbody>().velocity = force * direction;
    }
}
