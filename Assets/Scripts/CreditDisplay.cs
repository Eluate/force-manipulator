﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreditDisplay : MonoBehaviour {

    static void UpdateDisplay()
    {
        foreach (CreditDisplay creditDisplay in FindObjectsOfType<CreditDisplay>())
        {
            creditDisplay.Display();
        };
    }

	// Use this for initialization
	void Start () {
        Display();
	}
	
	public void Display () {
        GetComponent<Text>().text = "©" + PlayerPrefs.GetInt("Credits").ToString();
    }
}
