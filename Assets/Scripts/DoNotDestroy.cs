﻿using UnityEngine;
using System.Collections;

public class DoNotDestroy : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        DoNotDestroy[] destroyComponents = FindObjectsOfType(typeof(DoNotDestroy)) as DoNotDestroy[];
        for (int i = 0; i < destroyComponents.Length; i++)
        {
            if (this.GetInstanceID() != destroyComponents[i].GetInstanceID() && destroyComponents[i].gameObject.name == this.gameObject.name)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

}
