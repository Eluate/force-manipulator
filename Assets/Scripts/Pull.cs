﻿using UnityEngine;
using System.Collections;

public class Pull : MonoBehaviour
{

    public float strength;
    private Rigidbody otherBody;
    private CanBeForced otherCanBeForced;
    private GameObject otherObject;
    public bool isActive = true;

    void OnTriggerStay(Collider other)
    {
        if (!isActive)
        {
            return;
        }

        other.tag = "f";

        if (other.GetComponent<Rigidbody>() && other.GetComponent<CanBeForced>())
        {
            Vector3 distanceVector = other.transform.position - transform.position;
            float distance = distanceVector.sqrMagnitude / 8;
            if (distance < 1f)
            {
                distance = 1;
            }
            other.GetComponent<Rigidbody>().AddForce((distanceVector * strength) / distance, ForceMode.Acceleration);
        }
        if (otherObject && otherBody && otherCanBeForced && other.gameObject.GetInstanceID() == otherObject.GetInstanceID())
        {
            Vector3 distanceVector = other.transform.position - transform.position;
            float distance = distanceVector.sqrMagnitude / 8;
            if (distance < 1f)
            {
                distance = 1;
            }
            otherBody.AddForce((distanceVector * strength) / distance, ForceMode.Acceleration);
        }
        else
        {
            otherBody = other.GetComponent<Rigidbody>();
            otherCanBeForced = other.GetComponent<CanBeForced>();
            otherObject = other.gameObject;
            if (otherBody != null && otherCanBeForced != null)
            {
                Vector3 distanceVector = other.transform.position - transform.position;
                float distance = distanceVector.sqrMagnitude / 8;
                if (distance < 1f)
                {
                    distance = 1;
                }
                otherBody.AddForce((distanceVector * strength) / distance, ForceMode.Acceleration);
            }
        }
    }
}
