﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class MapGenerator : MonoBehaviour
{
    public static MapGenerator instance;

    private int level;
    private int coinCount;
    private int enemyCount;
    private float maxVector;
    private bool finished = false;

    public GameObject mapBasePrefab;
    public GameObject coinPrefab;

    public GameObject gameOverUI;
    public Text timerText;
    public Text creditText;

    public GameObject[] availableObstacles;
    public GameObject[] availableEnemies;

    public Text coinText;
    public Text enemyText;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        // Obtain next level and calculate map boundaries
        PlayerPrefs.SetInt("level", 0);
        level = PlayerPrefs.GetInt("level");
        level = level--;
        maxVector = (level + 5) * 2 + level;

        GenerateMap();
        GenerateEnemies();
        GenerateObstacles();
        GenerateCoins();
    }

    void GenerateCoins()
    {
        // Generate coins at random locations within the level boundaries
        coinCount = (int)(maxVector / 2.5f);
        for (int i = 0; i < coinCount; i++)
        {
            Instantiate(coinPrefab, GeneratePositionWithinBounds(), Quaternion.identity);    
        }

        coinText.text = coinCount.ToString();
    }

    void GenerateEnemies()
    {
        // Generate coins at random locations within the level boundaries
        enemyCount = (int)maxVector / 2;
        for (int i = 0; i < enemyCount; i++)
        {
            GameObject enemy = availableEnemies[Random.Range(0, availableEnemies.Length)];
            enemy = (GameObject)Instantiate(enemy, GeneratePositionWithinBounds(), Quaternion.identity);
        }

        enemyText.text = enemyCount.ToString();
    }

    void GenerateObstacles()
    {
        // Generate cubes at random locations between the boundaries
        for (int i = 0; i < maxVector * 5; i++)
        {
            GameObject obstacle = availableObstacles[Random.Range(0, availableObstacles.Length)];
            Instantiate(obstacle, GeneratePositionWithinBounds(), Quaternion.identity);
        }
    }

    Vector3 GeneratePositionWithinBounds()
    {
        var xPosition = Random.Range(-maxVector * 2.2f, maxVector * 2.2f);
        var yPosition = Random.Range(maxVector / 1.4f, maxVector * 4.4f);
        var zPosition = Random.Range(-maxVector * 2.2f, maxVector * 2.2f);

        if (xPosition < 0 && xPosition > -5)
        {
            xPosition = -5;
        }
        else if (xPosition > 0 && xPosition < 5)
        {
            xPosition = 5;
        }

        if (zPosition < 0 && zPosition > -5)
        {
            zPosition = -5;
        }
        else if (zPosition > 0 && zPosition < 5)
        {
            zPosition = 5;
        }

        return new Vector3(xPosition, yPosition, zPosition);
    }

    void GenerateMap()
    {
        // Create the map and scale it
        GameObject map = Instantiate(mapBasePrefab);
        map.transform.localScale = new Vector3(maxVector, maxVector, maxVector);
    }

    void LevelFinished()
    {
        if (finished)
        {
            return;
        }

        // Save the current level
        PlayerPrefs.SetInt("level", PlayerPrefs.GetInt("level") + 1);
        PlayerPrefs.Save();

        // Load the next level
        NextLevel nextLevel = gameObject.AddComponent<NextLevel>();
        nextLevel.loadSpecificLevel = 4;
        nextLevel.LoadLevel();
    }

    public void CoinDestroyed()
    {
        if (Time.timeSinceLevelLoad > 1)
        {
            // Play sound
        }
        // Decrement coin count
        coinCount--;
        // Update coin count in UI
        coinText.text = coinCount.ToString();

        if (coinCount <= 0)
        {
            LevelFinished();
        }
    }

    public void EnemyDestroyed()
    {
        if (Time.timeSinceLevelLoad > 1)
        {
            // Play sound
        }
        // Decrement enemy count
        enemyCount--;
        // Update enemy count in UI
        enemyText.text = enemyCount.ToString();

        if (enemyCount <= 0)
        {
            LevelFinished();
        }
    }

    public void PlayerKilled()
    {
        if (finished)
        {
            return;
        }

        // Find the player controller for component disable
        GameObject playerController = GameObject.Find("Player Controller");
        Destroy(playerController.GetComponent<MouseLook>());
        Destroy(playerController.GetComponent<RigidbodyFirstPersonController>());
        Destroy(playerController.GetComponent<Gun>());

        // Activate the UI for when the player dies
        foreach (Transform child in gameOverUI.transform.parent)
        {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
            child.gameObject.SetActive(false);
        }
        gameOverUI.SetActive(true);

        // Update timer text
        int secondsPassed = (int)Time.realtimeSinceStartup;

        if (secondsPassed / 60 != 1)
        {
            timerText.text = string.Format("You survived {0} minutes and {1} seconds", secondsPassed / 60, secondsPassed % 60);
        }
        else
        {
            timerText.text = string.Format("You survived {0} minute and {1} seconds", secondsPassed / 60, secondsPassed % 60);
        }

        // Update credit text
        int currentCredits = PlayerPrefs.GetInt("Credits");
        int gainedCoins = (int)(maxVector / 2.5f) - coinCount;
        creditText.text = string.Format("© {0} + {1}", currentCredits, gainedCoins);
        PlayerPrefs.SetInt("Credits", PlayerPrefs.GetInt("Credits") + gainedCoins);

        finished = true;
    }

}
