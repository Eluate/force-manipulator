﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	void OnCollisionEnter(Collision collision)
    {
        MapGenerator.instance.CoinDestroyed();
        Destroy(gameObject);
    }
}
