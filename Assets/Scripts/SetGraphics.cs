﻿using UnityEngine;
using System.Collections;

public class SetGraphics : MonoBehaviour
{

    public void SetSetting(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

}
