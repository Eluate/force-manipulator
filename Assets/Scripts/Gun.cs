﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    [System.Serializable]
    public class Config
    {
        public GameObject pullPrefab;
        public GameObject pushPrefab;
        
        public bool infiniteAmmo;
        
        public int pushAmmo;  
        public int pullAmmo;
        public int pushDelay;
        public int pullDelay;

        public float pushRange;
        public float pullRange;
        
        public Text pushText;   
        public Text pullText;

        public AudioClip pushSound;       
        public AudioClip pullSound;
        public AudioClip[] chargingSounds;
    }
    [SerializeField]
    private Config config;

    private GameObject pullObject;
    private GameObject pushObject;
    private GameObject pullInit = null;
    private GameObject pushInit = null;

    private Vector3 direction;
    private Vector3 startPosition;

    private Camera playCamera;
    private AudioSource audioSource;
    private bool charging = false;

    // Use this for initialization
    void Start()
    {
        playCamera = Camera.main;
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = 0.15f;
        if (config.pushText && config.pullText)
        {
            UpdateUI();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (config.infiniteAmmo)
        {
            if (config.pullAmmo < 1)
            {
                config.pullAmmo++;
            }
            if (config.pushAmmo < 1)
            {
                config.pushAmmo++;
            }
        }

        if (Input.GetButtonUp("Fire1"))
        {
            charging = false;

            Destroy(pullObject);

            if (pullInit != null)
            {
                pullInit.GetComponent<Pull>().isActive = true;
                pullObject = pullInit;
                pullInit = null;
            }
        }

        if (Input.GetButtonUp("Fire2"))
        {
            charging = false;

            Destroy(pushObject);

            if (pushInit != null)
            {
                pushInit.GetComponent<Push>().isActive = true;
                pushObject = pushInit;
                pushInit = null;
            }
        }

        if (charging)
        {
            return;
        }

        StartCoroutine(Listen());
    }

    IEnumerator Listen()
    {
        if (Input.GetButtonDown("Fire1") && config.pullAmmo > 0)
        {
            if (pullObject != null)
            {
                Destroy(pullObject);
            }

            if (pushInit != null)
            {
                Destroy(pushInit);
            }

            charging = true;

            for (int i = 0; i < config.pullDelay; i++)
            {
                if (!charging)
                {
                    i = config.pullDelay;
                }

                audioSource.clip = config.chargingSounds[Random.Range(0, config.chargingSounds.Length)];
                audioSource.Play();
                yield return new WaitForSeconds(0.3f);
            }

            if (charging)
            {
                charging = false;

                audioSource.clip = config.pullSound;
                audioSource.Play();

                pullInit = (GameObject)Instantiate(config.pullPrefab, transform.position + transform.forward, transform.rotation);
                pullInit.GetComponentInChildren<LookAt>().target = playCamera.transform;
                pullInit.GetComponent<Pull>().isActive = false;

                direction = playCamera.transform.forward;
                startPosition = gameObject.transform.position;

                config.pullAmmo--;
                UpdateUI();

                if (pushInit != null)
                {
                    pushInit.GetComponent<Push>().isActive = true;
                    pushObject = pushInit;
                    pushInit = null;
                }
            }
        }

        if (Input.GetButtonDown("Fire2") && config.pushAmmo > 0)
        {
            if (pushObject != null)
            {
                Destroy(pushObject);
            }

            if (pushInit != null)
            {
                Destroy(pushInit);
            }

            charging = true;

            for (int i = 0; i < config.pushDelay; i++)
            {
                if (!charging)
                {
                    i = config.pullDelay;
                }

                audioSource.clip = config.chargingSounds[Random.Range(0, config.chargingSounds.Length)];
                audioSource.Play();
                yield return new WaitForSeconds(0.3f);
            }

            if (charging)
            {
                charging = false;

                audioSource.clip = config.pushSound;
                audioSource.Play();

                pushInit = (GameObject)Instantiate(config.pushPrefab, transform.position + transform.forward, transform.rotation);
                pushInit.GetComponentInChildren<LookAt>().target = playCamera.transform;
                pushInit.GetComponent<Push>().isActive = false;

                direction = playCamera.transform.forward;
                startPosition = gameObject.transform.position;

                config.pushAmmo--;
                UpdateUI();

                if (pullInit != null)
                {
                    pullInit.GetComponent<Pull>().isActive = true;
                    pullObject = pullInit;
                    pullInit = null;
                }
            }
        }

        MoveOrbs();

        yield return 0;
    }

    private void MoveOrbs()
    {
        if (pullInit != null)
        {
            if (Vector3.Distance(startPosition, pullInit.transform.position + direction) < config.pullRange)
            {
                pullInit.transform.position = Vector3.Lerp(pullInit.transform.position, pullInit.transform.position + direction, Time.deltaTime * 20);
            }
        }
        else if (pushInit != null)
        {
            if (Vector3.Distance(startPosition, pushInit.transform.position + direction) < config.pushRange)
            {
                pushInit.transform.position = Vector3.Lerp(pushInit.transform.position, pushInit.transform.position + direction, Time.deltaTime * 20);
            }
        }
    }

    private void UpdateUI()
    {
        if (config.infiniteAmmo)
        {
            config.pushText.text = "∞";
            config.pullText.text = "∞";
        }
        else
        {
            config.pushText.text = config.pushAmmo.ToString();
            config.pullText.text = config.pullAmmo.ToString();
        }
    }

}
