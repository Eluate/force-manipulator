﻿using UnityEngine;
using System.Collections;

public class Push : MonoBehaviour
{
    public float strength;
    private Rigidbody otherBody;
    private CanBeForced otherCanBeForced;
    private GameObject otherObject;
    public bool isActive = true;

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Untagged")
        {
            other.tag = "f";
        }
        if (!isActive)
        {
            return;
        }
        if (otherObject && otherBody && otherCanBeForced && other.gameObject.GetInstanceID() == otherObject.GetInstanceID())
        {
            Vector3 distanceVector = other.transform.position - transform.position;
            float distance = distanceVector.sqrMagnitude;
            otherBody.AddForce((distanceVector * strength) / (distance / 8), ForceMode.Acceleration);
        }
        else
        {
            otherBody = other.GetComponent<Rigidbody>();
            otherCanBeForced = other.GetComponent<CanBeForced>();
            otherObject = other.gameObject;
            if (otherBody != null && otherCanBeForced != null)
            {
                Vector3 distanceVector = other.transform.position - transform.position;
                float distance = distanceVector.sqrMagnitude;
                other.GetComponent<Rigidbody>().AddForce((distanceVector * strength) / (distance / 8), ForceMode.Acceleration);
            }
        }
    }
}
