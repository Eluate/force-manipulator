﻿using UnityEngine;
using System.Collections;

public class TriggerPosition : MonoBehaviour
{

    public bool playerOnly = true;
    public Vector3 positionToSet = new Vector3(0, 2, 0);

    void OnTriggerEnter(Collider other)
    {
        if (playerOnly)
        {
            if (other.gameObject.tag == "Player")
            {
                other.gameObject.transform.position = positionToSet;
            }
        }
        else
        {
            other.gameObject.transform.position = positionToSet;
        }
    }
}
