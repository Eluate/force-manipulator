﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Settings : MonoBehaviour
{

    public Slider soundSlider;
    public Slider musicSlider;
    public Slider sensitivitySlider;

    public AudioSource musicSource;

    void OnLevelLoaded(int level)
    {
        if (level != 0)
        {
            return;
        }

        soundSlider.value = PlayerPrefs.GetFloat("sound");
        musicSlider.value = PlayerPrefs.GetFloat("music");
        sensitivitySlider.value = PlayerPrefs.GetFloat("sensitivity");

        AudioListener.volume = soundSlider.value;
        musicSource.volume = musicSlider.value;
    }

    public void SetSensitivity()
    {
        PlayerPrefs.SetFloat("sensitivity", sensitivitySlider.value);
    }

    public void SetSound()
    {
        PlayerPrefs.SetFloat("sound", soundSlider.value);
        AudioListener.volume = soundSlider.value;
    }

    public void SetMusic()
    {
        PlayerPrefs.SetFloat("music", musicSlider.value);
        musicSource.volume = musicSlider.value;
    }
}
